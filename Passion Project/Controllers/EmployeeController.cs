﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Passion_Project.Models;
using System.Net;
using System.Data.Entity;

namespace Passion_Project.Controllers
{
    public class EmployeeController : Controller
    {
        private CompanyEmployeeContent db = new CompanyEmployeeContent();
        // GET: Employee
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeId,EmployeeName,EmployeeEmail,EmployeeSalary,EmployeeHireDate,EmployeeAddress,EmployeeHours")] Employee emply)
        {
            if (ModelState.IsValid)
            {
                db.Employee.Add(emply);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(emply);
        }

        public ActionResult List()
        {
            return View(db.Employee.ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee emply = db.Employee.Find(id);
            if (emply == null)
            {
                return HttpNotFound();
            }
            return View(emply);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeId,EmployeeName,EmployeeEmail,EmployeeSalary,EmployeeHireDate,EmployeeAddress,EmployeeHours")] Employee emply)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emply).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(emply);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee emply = db.Employee.Find(id);
            if (emply == null)
            {
                return HttpNotFound();
            }
            return View(emply);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee emply = db.Employee.Find(id);
            if (emply == null)
            {
                return HttpNotFound();
            }
            return View(emply);
        }

        // POST:Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee emply = db.Employee.Find(id);
            db.Employee.Remove(emply);
            db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}