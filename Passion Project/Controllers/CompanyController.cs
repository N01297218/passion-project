﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Passion_Project.Models;
using System.Net;
using System.Data.Entity;

namespace Passion_Project.Controllers
{
    public class CompanyController : Controller
    {
        private CompanyEmployeeContent db = new CompanyEmployeeContent();
        // GET: Company
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CompanyId,CompanyName,CompanyDescription,CompanyAddress")] Company comp)
        {
            if (ModelState.IsValid)
            {
                db.Company.Add(comp);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(comp);
        }

        public ActionResult List()
        {
            return View(db.Company.ToList());
        }
        //Edit
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company comp = db.Company.Find(id);
            if (comp == null)
            {
                return HttpNotFound();
            }
            return View(comp);
        }

        // POST: Company/Edit
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CompanyId,CompanyName,CompanyDescription,CompanyAddress")] Company comp)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(comp);
        }

        //Details
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company comp = db.Company.Find(id);
            if (comp == null)
            {
                return HttpNotFound();
            }
            return View(comp);
        }
        //Delete

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company comp = db.Company.Find(id);
            if (comp == null)
            {
                return HttpNotFound();
            }
            return View(comp);
        }

        // POST:Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company comp = db.Company.Find(id);
            db.Company.Remove(comp);
            db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}