﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Passion_Project.Models
{
    public class Employee
    {

        [Key]
        public int EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeEmail { get; set; }

        public string EmployeeSalary { get; set; }

        public string EmployeeHireDate { get; set; }

        public string EmployeeAddress { get; set; }

        public string EmployeeHours { get; set; }
    }
}