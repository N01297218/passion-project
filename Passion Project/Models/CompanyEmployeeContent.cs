﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Passion_Project.Models
{
    public class CompanyEmployeeContent:DbContext
    {
        
        public CompanyEmployeeContent()
        {

        }
        public DbSet<Company>Company { get; set; }
        public DbSet<Employee>Employee { get; set; }
    }
}